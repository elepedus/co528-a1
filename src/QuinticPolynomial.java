/**
 * University of Kent 2016
 * CO528 Assessment 1
 * Alex Burbidge, Chris Cunningham, Edmond Lepedus
 * March 2016
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

/**
 * Quantic Polynomial class - Stores the mutation methods and
 * polynomial (genotype) of the current solution
 */
public class QuinticPolynomial implements Comparable<QuinticPolynomial> {

    public static float crossoverProbability = 0.5f;
    public static float mutationProbability = 0.1f;

    private float[] minimumValues;
    private float[] maximumValues;

    public static boolean useSeed = false;

    public double error;

    private static Random random = new Random();

    private double[] genotype;
    private double[] outputs;

    /*
     * Ie, the graph
     */
    public QuinticPolynomial() {
        /**
         * Using heuristic analysis - ie looking at the graph
         * backwards, we can tell some basic factors about the
         * graph. Firstly, the value for A must be near zero, and
         * the values for B must be a reasonably large positive
         * (as by eye the entire plot is in the positive axis)
         * while the values for C D may vary but are smaller than
         * are found earlier in B. E, F - Arguably the two most
         * significant variables for matching the curve, can be
         * estimated to be extremely small (and at least one of
         * them negative) otherwise the tail of the curve would
         * be pointing upwards from the X axis, rather than
         * downward
         */
        // Symmetric ranges seem to work best
        minimumValues = new float[]{-100, -10000, -1000, -100, -5, -0.1f};
        maximumValues = new float[]{100, 10000, 1000, 100, 5, 0.1f};

        error = Integer.MAX_VALUE;

        // Allows us to resume a run.
        if (useSeed) {
            genotype = new double[]{
                    -5664.82433941057,
                    -5569.00872027161,
                    -6275.3608612237695,
                    685.1574109737962,
                    -1.0051192617695506,
                    0.0003383204490319258
            };
        } else {
            initialiseValues();
        }
    }

    /**
     * Constructor class
     *
     * @param source
     */
    public QuinticPolynomial(QuinticPolynomial source) {
        minimumValues = source.minimumValues.clone();
        maximumValues = source.maximumValues.clone();
        error = source.error;
        genotype = source.genotype.clone();
    }

    /**
     * Generate new random population
     *
     * @param populationSize Size for the new population
     * @return an array of new citizens
     */
    public static ArrayList<QuinticPolynomial> generatePopulation(int populationSize) {
        ArrayList<QuinticPolynomial> population = new ArrayList<QuinticPolynomial>(populationSize);
        for (int i = 0; i < populationSize; i++) {
            population.add(new QuinticPolynomial());
        }
        return population;
    }

    /**
     * Initialise new genotypes
     */
    private void initialiseValues() {
        genotype = new double[6];

        for (int i = 0; i < genotype.length; i++) {
            genotype[i] = getNewGeneValue(i);
        }
    }

    /**
     * Get new Gene Value
     *
     * @param geneIndex
     * @return
     */
    private float getNewGeneValue(int geneIndex) {
        return random.nextFloat() * (maximumValues[geneIndex] - minimumValues[geneIndex]) + minimumValues[geneIndex];
    }

    /**
     * Evalute test data against the expected outputs
     *
     * @param test_data        Current population Kn
     * @param expected_outputs The expected data loaded in via csv
     */
    public void evaluate(double[] test_data, double[] expected_outputs) {
        error = meanPercentageError(deriveOutputs(test_data), expected_outputs);
    }

    /**
     * Calculate the Mean SE for a candidate
     * note, only one of these may be used!
     *
     * @param candidate_outputs Potential outputs
     * @param expected_outputs  Target outputs
     * @return the MSE
     */
    private double meanSquaredError(double[] candidate_outputs, double[] expected_outputs) {
        return sumSquaredError(candidate_outputs, expected_outputs) / expected_outputs.length;
    }

    /**
     * Calculate the SSE for a candidate
     * note, only one of these may be used!
     *
     * @param candidate_outputs Potential outputs
     * @param expected_outputs  Target outputs
     * @return the SSE
     */
    private double sumSquaredError(double[] candidate_outputs, double[] expected_outputs) {
        double sum = 0;

        for (int i = 0; i < expected_outputs.length; i++) {
            sum += Math.pow(expected_outputs[i] - candidate_outputs[i], 2);
        }
        return sum;
    }

    /**
     * Calculate the CSE for a candidate
     * note, only one of these may be used!
     *
     * @param candidateOutputs Potential outputs
     * @param expectedOutputs  Target outputs
     * @return the CSE
     */
    private double sumChiSquaredError(double[] candidateOutputs, double[] expectedOutputs) {
        double sum = 0;
        for (int i = 0; i < expectedOutputs.length; i++) {
            sum += Math.pow(candidateOutputs[i] - expectedOutputs[i], 2) / expectedOutputs[i];
        }
        return sum;
    }

    /**
     * Calculate the MPE for a candidate
     * note, only one of these may be used!
     *
     * @param candidateOutputs Potential outputs
     * @param expectedOutputs  Target outputs
     * @return the MPE
     */
    private double meanPercentageError(double[] candidateOutputs, double[] expectedOutputs) {
        int count = 0;
        double sum = 0;
        for (int i = 0; i < expectedOutputs.length; i++) {
            count++;
            sum += Math.abs((expectedOutputs[i] - candidateOutputs[i]) / expectedOutputs[i]) * 100;
        }
        return sum / count;
    }

    /**
     * Derive plots of y from x values stored in files
     *
     * @param testData
     * @return plots of y
     */
    public double[] deriveOutputs(double[] testData) {
        outputs = new double[testData.length];

        for (int i = 0; i < testData.length; i++) {
            double x = testData[i];
            outputs[i] = hornersRule(x);

        }
        return outputs;
    }

    /**
     * Horner's Rule - Faster implementation for polynomial calculation
     *
     * @param x integer
     * @return evaluation of the polynomial (y)
     */
    private double hornersRule(double x) {
        double result = 0.0;

        for (int j = genotype.length - 1; j > -1; j--) {
            result = genotype[j] + x * result;
        }
        return result;
    }

    /**
     * Calculate the fitness
     *
     * @return the fitness as a value tending towards 1
     */
    public double getFitness() {
        return 1 / (1 + error / 100);
    }

    /**
     * Multiple Crossover Mutation
     *
     * @param parent2 Parent to be mutated with
     * @return Child of the mutation
     */
    public QuinticPolynomial[] multiTypeCrossover(QuinticPolynomial parent2) {
        if (random.nextFloat() < 0.5) {
            return uniformCrossover(parent2);
        } else {
            return singlePointCrossover(parent2);
        }
    }

    /**
     * Crossover Mutation Method
     *
     * @param parent2
     * @return
     */
    public QuinticPolynomial[] uniformCrossover(QuinticPolynomial parent2) {

        QuinticPolynomial[] offspring = new QuinticPolynomial[2];

        offspring[0] = new QuinticPolynomial();
        offspring[0].genotype = this.genotype.clone();
        offspring[1] = new QuinticPolynomial();
        offspring[1].genotype = parent2.genotype.clone();

        for (int i = 0; i < genotype.length; i++) {
            if (random.nextFloat() < crossoverProbability) {
                offspring[0].genotype[i] = parent2.genotype[i];
                offspring[1].genotype[i] = this.genotype[i];
            }
        }

        return offspring;
    }

    /**
     * Singe point crossover method
     *
     * @param parent2 Parent to cross with
     * @return A new polynomial
     */
    public QuinticPolynomial[] singlePointCrossover(QuinticPolynomial parent2) {
        QuinticPolynomial[] offspring = new QuinticPolynomial[2];

        offspring[0] = new QuinticPolynomial(this);
        offspring[1] = new QuinticPolynomial(parent2);

        int crossoverPoint = random.nextInt(6);

        for (int i = 0; i < genotype.length; i++) {
            if (i <= crossoverPoint) {
                offspring[0].genotype[i] = parent2.genotype[i];
                offspring[1].genotype[i] = genotype[i];
            }
        }

        return offspring;
    }

    /**
     * Apply a point Mutate
     */
    public void pointMutate() {
        pointMutate(0.7, false);
    }

    /**
     * Point Mutate
     *
     * @param relativeMutationThreshold
     * @param useRounding
     */
    public void pointMutate(double relativeMutationThreshold, boolean useRounding) {
        if (random.nextFloat() < mutationProbability) {
            int mutations = random.nextInt(7);

            for (int i = 0; i < mutations; i++) {
                int geneIndex = random.nextInt(6);
                float roll = random.nextFloat();

                if (roll < relativeMutationThreshold) {
                    relativeMutation(geneIndex);
                } else if (roll < 0.775) {
                    massiveMutation(geneIndex);
                } else if (roll < 0.850) {
                    randomMutation(geneIndex);
                } else if (roll < 0.925) {
                    negate(geneIndex);
                } else {
                    gaussianResetMutation(geneIndex);
                }
                if (useRounding && roll < 0.5) {
                    roundMutation(geneIndex);
                }
            }
        }
    }

    /**
     * Resets the gene value to a random value from
     * a normal distribution with mean 0 and standard
     * deviation 1.
     *
     * @param geneIndex
     */
    public void gaussianResetMutation(int geneIndex) {
        genotype[geneIndex] = random.nextGaussian();
    }

    /**
     * Resets the gene value to a random value in its
     * designated range
     *
     * @param geneIndex
     */
    public void randomMutation(int geneIndex) {
        genotype[geneIndex] = random.nextDouble() * (maximumValues[geneIndex] - minimumValues[geneIndex]) + minimumValues[geneIndex];
    }

    /**
     * Replaces the gene value with a new value sampled
     * from a normal distribution centered on its current
     * value and a relatively small standard deviation
     *
     * @param geneIndex
     */
    public void relativeMutation(int geneIndex) {
        double mean = genotype[geneIndex];
        double standardDeviation = Math.abs(mean / 20);

        genotype[geneIndex] = Double.max(
                Double.min(random.nextGaussian() * standardDeviation + mean,
                        maximumValues[geneIndex]),
                minimumValues[geneIndex]
        );
    }

    /**
     * Massively modify the gene's value
     *
     * @param geneIndex
     */
    public void massiveMutation(int geneIndex) {
        double range = (maximumValues[geneIndex] - minimumValues[geneIndex]) / 5;
        double delta = random.nextDouble() * range;

        if (random.nextFloat() < 0.5) {
            delta = -delta;
        }

        genotype[geneIndex] = Double.max(Double.min(genotype[geneIndex] + delta, maximumValues[geneIndex]), minimumValues[geneIndex]);
    }

    /**
     * Negation mutator to offset from gaussian on tiny values!
     *
     * @param geneIndex Gene at X
     */
    public void negate(int geneIndex) {
        genotype[geneIndex] = -genotype[geneIndex];
    }

    /**
     * Round off a variable
     *
     * @param geneIndex
     */
    public void roundMutation(int geneIndex) {
        BigDecimal bd = new BigDecimal(genotype[geneIndex]);
        bd = bd.round(new MathContext(random.nextInt(4) + 1));
        genotype[geneIndex] = bd.doubleValue();
    }

    /**
     * To string method for the class
     *
     * @return a readable string representation of this polynomial
     */
    public String toString() {
        return "Fitness: " + this.getFitness() +
                "% Error: " + error + "% " + Arrays.toString(genotype);
    }

    /**
     * Compare to overload for comparing two polynomials
     *
     * @param o Another Quintic Polynomial
     * @return a difference in fitness
     */
    @Override
    public int compareTo(QuinticPolynomial o) {
        return Double.compare(this.getFitness(), o.getFitness());
    }

    /**
     * Write the best output to file
     *
     * @param test_data the test values
     */
    public void writeOutputsToFile(double[] test_data) {
        BufferedWriter br;
        try {
            br = new BufferedWriter(
                    new FileWriter("bestOutput" +
                            System.currentTimeMillis() + ".dat", false));
            for (int i = 0; i < test_data.length; i++) {
                br.write(test_data[i] + "\t" + hornersRule(test_data[i]) + "\n");
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}