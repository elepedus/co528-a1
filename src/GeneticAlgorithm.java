/**
 * University of Kent 2016
 * CO528 Assessment 1
 * Alex Burbidge, Chris Cunningham, Edmond Lepedus
 * March 2016
 */

import com.sun.tools.javac.util.List;

import java.io.*;
import java.util.*;

/**
 * Genetic Algorithm - Assessment 1 - Intelligent Systems
 */
public class GeneticAlgorithm {

    ArrayList<QuinticPolynomial> population;

    boolean elitist;

    double[] test_data;
    double[] expected_output;

    float selectivePressure;
    float exploratoryPressure;

    int populationSize;
    int ageOfBestSolution;
    int outputInterval;
    int generations;
    int k = 108;
    int min_k = 2;
    int nominalK = 108;
    int nominalPopulationSize;

    Map<Integer, Map> runStats;

    Random random;

    QuinticPolynomial bestSolution;
    QuinticPolynomial bestInLastGen;

    /**
     * Call itself with default arguments
     */
    public GeneticAlgorithm()
    {
        this(120, Integer.MAX_VALUE, "data.csv", true);

    }

    /**
     * Runs a genetic algorithm
     *
     * @param populationSize Size of the population
     * @param generations
     * @param inputPath
     * @param elitist
     */
    public GeneticAlgorithm(int populationSize, int generations, String inputPath, boolean elitist) {
        nominalPopulationSize = populationSize;

        this.populationSize = nominalPopulationSize;
        this.generations = generations;
        this.elitist = elitist;

        outputInterval = 1000;
        bestSolution = null;
        ageOfBestSolution = 0;
        selectivePressure = 0f;

        random = new Random();
        runStats = new HashMap<>();
        population = QuinticPolynomial.generatePopulation(populationSize);
        loadData(inputPath);
    }

    /**
     * Sets up a new GA
     *
     * @param args from runtime
     */
    public static void main(String[] args) {
        QuinticPolynomial.useSeed = false;
        GeneticAlgorithm ga = new GeneticAlgorithm();
        ga.run();
    }

    /**
     * Run a GA for a number of generations
     */
    public void run() {
        for (int i = 1; i <= generations; i++) {
            evaluatePopulation();
            saveStats(i);
            bestInLastGen = Collections.max(population);
            if (bestSolution == null ||
                    bestSolution.compareTo(bestInLastGen) < 0) {
                bestSolution = new QuinticPolynomial(bestInLastGen);
                ageOfBestSolution = 0;
                k = nominalK;
            } else {
                ageOfBestSolution++;
            }

            // Print fitness statement every so often
            if (i % outputInterval == 0) {
                outputStatistics(i);
            }

            if (bestSolution.error < 0.000001) {
                outputStatistics(i);
                writeRunStatsToFile();
                bestSolution.writeOutputsToFile(test_data);
                break;
            }

            // Run updates
            updateExploratoryPressure(ageOfBestSolution);
            updateSelectivePressure(i, ageOfBestSolution);
            generateNewPopulation();
        }
    }


    /**
     * Parallelled evaluation of the total population for
     * generation k
     */
    private void evaluatePopulation() {
        population.parallelStream().forEach(c -> c.evaluate(test_data, expected_output));
    }

    /**
     * Save the statistics for a particular generation
     *
     * @param generation Generation Number for statistics to be saved
     */
    private void saveStats(int generation) {
        double bestScore = Collections.max(population).getFitness();
        double worstScore = Collections.min(population).getFitness();
        double averageScore = population.stream().
                mapToDouble(QuinticPolynomial::getFitness).
                average().getAsDouble();

        Map<String, Double> generationStats = new HashMap<>();

        generationStats.put("worst", worstScore);
        generationStats.put("average", averageScore);
        generationStats.put("best", bestScore);

        runStats.put(generation, generationStats);
    }

    /**
     * Increase the exploratory pressure dependant on the amount of
     * time since the last major change
     * @param ageOfBestSolution The age of the solution
     */
    private void updateExploratoryPressure(int ageOfBestSolution) {
        exploratoryPressure = ageOfBestSolution / 1000;

        if (exploratoryPressure < 1) {
            QuinticPolynomial.mutationProbability = 0.1f;
        } else {
            QuinticPolynomial.mutationProbability = Float.min(1,
                    QuinticPolynomial.mutationProbability * exploratoryPressure);
        }
    }

    /**
     * Update the selective pressure on the age of the best fit
     *
     * @param currentGeneration the current generation
     * @param ageOfBestSolution the age of the best fit
     */
    private void updateSelectivePressure(int currentGeneration, int ageOfBestSolution) {
        if (ageOfBestSolution > 50000) {
            sinVarySelectivePressure(currentGeneration);
        }
    }

    /**
     * Vary the selection pressure in a sin curve pattern, e.g.
     * 1, 0, -1, 0, 1, 0, -1
     *
     * @param currentGeneration
     */
    private void sinVarySelectivePressure(int currentGeneration) {
        selectivePressure = (float) Math.pow(Math.sin(currentGeneration / 100), 2);
        k = Math.round(Float.max(min_k, selectivePressure * populationSize));
    }

    /**
     * Linearly increase the selection pressure
     *
     * @param currentGeneration the current generation
     * @param ageOfBestSolution the age of the best fit
     */
    private void linearSelectivePressureRamp(int currentGeneration, int ageOfBestSolution) {
        // Should be betweeen 10 and 90%
        selectivePressure = Float.min(0.95f, 0.1f + (currentGeneration / 1000000f));

        // Add up to 25% of the difference in random noise
        float noise = (1 - selectivePressure) * (random.nextFloat() / 4);

        k = Math.round(populationSize * selectivePressure + noise);
    }

    /**
     * Generate a new population (k)
     */
    private void generateNewPopulation() {
        ArrayList<QuinticPolynomial> new_population = new ArrayList<>(populationSize);

        if (elitist) {
            new_population.add(new QuinticPolynomial(bestSolution));
            new_population.add(new QuinticPolynomial(bestInLastGen));
        }

        if (ageOfBestSolution > 10000) {
            new_population.addAll(QuinticPolynomial.generatePopulation(
                    (int) Math.round(populationSize * 0.1 *
                            Math.log10(ageOfBestSolution))));
        }

        while (new_population.size() < populationSize) {
            QuinticPolynomial[] parents = selectParents();
            QuinticPolynomial[] children = generateOffspring(parents[0], parents[1]);
            mutate(children);
            new_population.addAll(List.from(children));
        }
        population = new_population;
    }

    /**
     * Generic Mutator Method
     *
     * @param children
     */
    private void mutate(QuinticPolynomial[] children) {
        for (QuinticPolynomial child : children) {
            child.pointMutate(Double.max(0.7, 1 - bestSolution.getFitness()),
                    (bestSolution.error < 0.1)
            );
        }
    }

    /**
     * Generate offspring from two parents using multitype crossover
     *
     * @param parent1
     * @param parent2
     * @return
     */
    private QuinticPolynomial[] generateOffspring(QuinticPolynomial parent1, QuinticPolynomial parent2) {
        return parent1.multiTypeCrossover(parent2);
    }

    /**
     * Randomly generate two new parents
     *
     * @return Array of QuinticPolynomial
     */
    private QuinticPolynomial[] selectParents() {
        QuinticPolynomial[] parents = new QuinticPolynomial[2];
        parents[0] = selectParent();
        parents[1] = selectParent();
        return parents;
    }

    /**
     * return a parent from k (the selection)
     *
     * @return
     */
    private QuinticPolynomial selectParent() {
        return tournamentSelection(k);
    }

    /**
     * Tournament Selection
     *
     * @param k
     * @return
     */
    private QuinticPolynomial tournamentSelection(int k) {
        QuinticPolynomial winner = null;
        for (int i = 0; i < k; i++) {
            QuinticPolynomial challenger = population.get(Math.round(
                    random.nextFloat() * (population.size() - 1))
            );

            if (winner == null) {
                winner = challenger;
            } else if (winner.getFitness() < challenger.getFitness()) {
                winner = challenger;
            }
        }

        population.remove(winner);

        return winner;
    }

    /**
     * Load the raw data from a CSV
     *
     * @param inputPath the location of the csv file
     */
    private void loadData(String inputPath) {
        BufferedReader br = null;
        String line;

        LinkedList<Double> inputs = new LinkedList<>();
        LinkedList<Double> outputs = new LinkedList<>();

        try {
            br = new BufferedReader(new FileReader(inputPath));

            while (null != (line = br.readLine())) {
                // use comma as separator
                String[] data_point = line.split(",");

                inputs.add(Double.parseDouble(data_point[0]));
                outputs.add(Double.parseDouble(data_point[1]));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        test_data = getDoubleArray(inputs);
        expected_output = getDoubleArray(outputs);
    }

    /**
     * Output statistics to the console
     *
     * @param generationNumber Generation Number for statistics to be saved
     */
    private void outputStatistics(int generationNumber) {
        System.out.println(
                "Generation: " +
                        generationNumber +
                        " k: " +
                        k +
                        " Age of best solution: " +
                        ageOfBestSolution +
                        " pMut: " +
                        QuinticPolynomial.mutationProbability +
                        " N:" +
                        populationSize
        );

        System.out.println(bestSolution);

        if (!elitist) {
            System.out.println("Best in gen: " + bestInLastGen);
        }
    }

    /**
     * Write the statistics into a file, Ready to be read into LaTeX
     */
    private void writeRunStatsToFile() {
        BufferedWriter br;
        // Limit the size of the file,
        // otherwise LaTeX runs out of memory xD
        int interval = runStats.size() / 1000;
        int i = 0;
        try {
            br = new BufferedWriter(
                    new FileWriter("runstats" + System.currentTimeMillis() + ".csv", false));
            br.write("Generation, Worst Fitness, " + "Average Fitness, Best Fitness \n");
            for (int gen : runStats.keySet()) {
                if (i % interval == 0) {
                    String sb = String.valueOf(gen) +
                            ", " + runStats.get(gen).get("worst") +
                            ", " + runStats.get(gen).get("average") +
                            ", " + runStats.get(gen).get("best") +
                            "\n";
                    br.write(sb);
                }
                i++;
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get double array
     *
     * @param list
     * @return
     */
    public static double[] getDoubleArray(LinkedList<Double> list) {
        double[] array = new double[list.size()];

        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }

        return array;
    }
}