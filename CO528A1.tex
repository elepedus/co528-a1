\input{preamble.tex}
\section*{Document 1: Source Code}
\lstset{
  basicstyle=\ttfamily,
  numbers=left,    
  firstnumber=1,
  numberfirstline=true,
  numberstyle=\tiny,
}
\lstinputlisting[caption=GeneticAlgorithm.java, language=Java, breaklines]{src/GeneticAlgorithm.java}
\lstinputlisting[caption=QuinticPolynomial.java, language=Java, breaklines]{src/QuinticPolynomial.java}

\pagebreak

\section*{Document 2: Solution Description \& Results}

\subsection*{a. Plot of downloaded data points}

\vspace{1cm}

\begin{tikzpicture}
	\begin{axis}[title={Downloaded data}, xlabel = x, ylabel = y, width=15cm]
		\addplot table {datfile.dat}	;
	\end{axis}

\end{tikzpicture}

\pagebreak
\subsection*{b. Description of GA}
\subsubsection*{Candidate Representation}
The solution was represented as an array of 6 double precision floating point numbers, one for each coefficient, contained within an object. An array of doubles was chosen as both these types incur almost no performance overhead, while ensuring that a high degree of accuracy is obtainable. 'Wrapping' the genotype in an object does have some effect on performance, but it allows us to attach additional metadata and behaviour to the candidate solution, which simplifies the implementation.

One of the most important pieces of metadata we attach to the object is a set of minimum and maximum values for each gene, which allow us to greatly reduce the search space based on heuristic analysis of the target graph.
\subsubsection*{Fitness Function}
The choice of fitness function appears to have been critical in this case, and required a degree of trial and error. We decided from the very beginning that fitness should be between 0 and 1 with better candidates having a higher fitness, since having a low fitness score for a good candidate is counter-intuitive and error-prone. 

We initially tried $\frac{1}{1+SumSquared Errors}$, soon followed by $\frac{1}{1+Mean Squared Error}$, but found that they were both dominated by errors in the upper part of the function's range, and did not give a good indication of how close we were to a solution. 

We briefly considered using $Root Mean Squared Error$, but the performance overhead of taking the square root did not appeal.

We eventually settled on $\frac{1}{1+(Mean Absolute Percentage Error \div 100)}$. While any of the other measures would have given us a fitness of ~1 for a perfect solution, this measure had the advantage of giving us a meaningful gauge of how far from perfect a given solution was. This proved to make a huge difference to our algorithm, as it forced the selection of values which improved the solution across the entire domain of the function, rather than just at the upper end, where a small percentage difference can completely dominate SSE or MSE due to the large numbers involved.
\subsubsection*{Selection Algorithm}
We initially tried Roulette Wheel Selection, followed by Rank-Based Selection, but quickly swapped to Tournament Selection, as it facilitates adjustments to the selection pressure, which is crucial for minimising run-time.
\subsubsection*{Crossover Operators}
We tried single-point crossover, two-point crossover and uniform crossover, but eventually decided to use both single-point and uniform crossover with equal probability, based on the conclusions in \citep{MagalhaesMendes:2013ux}, where they were found to be the best crossover operators.
\subsubsection*{Mutation Operators}
Our final implementation uses a number of different mutation operators, and varies some of their probabilities depending on the fitness achieved at that point. 

Usually we apply a relative mutation with an adaptive probability dependant on the fitness of the best solution found so far, starting at 0.7. This operator samples a normal distribution centred on the gene's current value with a standard deviation of 1/20th of that value. This mutation is designed to ensure that we thoroughly explore the space around our solution.

Alternatively, while the fitness of the best candidate is less than 0.775, there is a 0.075 chance of a massive mutation, which changes the current value of the gene by up to 20\% of the gene's defined range.

Similarly, while the fitness of the best candidate is less than 0.850, there is a 0.075 probability that the gene value will be replaced with a new value. This is meant to provide an occasional 'kick' to genes which might otherwise have become stuck in a local optimum.

Likewise, while the fitness of the best candidate is less than 0.925, there is a 0.075 chance that its sign will flip. The main purpose of this operator is to cover for the breakdown of relative mutation as values approach zero.

Moreover, we apply a Gaussian reset mutation with up to 0.075 probability, which simply replaces the gene's value with a random sample from a normal distribution with mean 0 and standard deviation 1.

Finally, if the best candidate's fitness is greater than 0.999, we apply a rounding mutation with an independent probability of 0.5. This rounds a gene to a randomly chosen number of significant figures between 1 and 4. This operator is essential to bypassing the inordinate amount of time usually spent optimising the last fraction of a percent.

All the mutations were applied to a random number of genes for each candidate.

\subsubsection*{Parameters and Optimisations}
All the parameter choices are guided by two fundamental insights about genetic algorithms:
\begin{enumerate}
	\item {Performance is king. All else being equal, a faster algorithm will allow us to generate and evaluate more candidate solutions in a shorter time. Shorter run times also enable us to iteratively tweak our parameters to achieve optimum results.}
	\item {GA performance is largely dictated by the balance between exploitation and exploration}
\end{enumerate}
\subsubsection*{Population Size and Tournament Size}
During development, we observed that a tournament size equal to 90\% of the population provided a massive increase in convergence speed without negatively impacting on solution quality. However, complexity analysis of our tournament selection implementation indicated that as the tournament size approaches the population size, the complexity approaches $O(n^2)$, and associated increases in run time negate any gains. A population size of 120 and tournament size of 108 was found to offer a good balance.

The genetic algorithm implementation triggers cyclical variation in tournament size if the best solution has not changed in over 50000 generations, in order to facilitate escape from local optimum. The variations follow a $sin^2$ curve, with a minimum tournament size of 2 and a maximum of 120. The cyclic variation stops when the best solution improves, and the tournament size returns to 108.

A linear ramp-up of population size was also tested, but we found that it was very prone to getting stuck in local optima as the run progressed.

\subsubsection*{Crossover and Mutation Rates}
The crossover rate was set at 0.5. The starting mutation rate was set at 0.1, but our genetic algorithm gradually increases mutation rate when the best solution has not improved for more than 1000 generations. The mutation rate reaches 1 when the best solution has not improved for 10000 generations, and is reset to 0.1 as soon as the best solution improves.

\subsubsection*{Elitism}
We found that elitism was key to achieving good performance, as it allowed out GA to explore widely without any chance of losing an optimal solution. We implemented an extension to elitism to preserve both the best solution ever found, and the best solution in the last generation. This seemed to work better, perhaps because it provided a degree of exploratory continuity between generations.

\subsubsection*{Random Variation}
When the age of the best candidate is over 10000, we introduce randomly initialised new candidates into the next generation. The number of new candidates introduced starts at 10\% of the population size and increases logarithmically with the age of the best solution, theoretically reaching 100\% if the best solution has not improved in $10^10$ generations.

\subsubsection*{Horner's Rule}
It was found that using Horner's rule when generating the candidates' phenotypes resulted in a dramatic increase in the speed of execution over naïvely computing powers.

\subsubsection*{Concurrency}
We used Java 8 parallel streams to enable concurrent evaluation of candidates, which resulted in a threefold increase in execution speed on a quad core machine with hyper-threading.


\subsection*{c. Plot showing best, worst \& average fitness changes over log time}
%\tikzsetnextfilename{g1}
\begin{tikzpicture}
	\begin{semilogxaxis}[title={Best, worst and average fitness over time}, xlabel = Generation, ylabel = Fitness, width=13cm, height=20cm, xmin=0, legend pos=outer north east]
		\addlegendentry{Worst Fitness}
		\addplot table [y=Worst Fitness, x=Generation, col sep=comma] {runstats1457032545246.csv}	;
		\addlegendentry{Average Fitness}
		\addplot table [y=Average Fitness, x=Generation, col sep=comma] {runstats1457032545246.csv}	;
		\addlegendentry{Best Fitness}
		\addplot table [y=Best Fitness, x=Generation, col sep=comma] {runstats1457032545246.csv}	;

	\end{semilogxaxis}

\end{tikzpicture}

\subsection*{d. A formulation of the best solution}
\begin{equation*}
\def\a {3.889E-4}
\def\b {5000.0}
\def\c {5.0}
\def\d {-62.0}
\def\e {1.0}
\def\f {-0.001}
	\a + \b x + \c x^2 + \d x^3 + \e x^4 + \f x^5
\end{equation*}

\subsection*{e. A plot containing both the best solution found \& the original downloaded data}
\begin{tikzpicture}
	\begin{axis}[title={Best Solution vs Downloaded Data}, xlabel = x, ylabel = y, width=13cm, legend pos=outer north east, legend style={draw=none, mark=none}]
		\addplot [densely dotted, mark=none, blue, ultra thick] table {datfile.dat}	;
		\addplot [loosely dashed, mark=none, red, ultra thick] table {bestOutput1457032545265.dat};
		\legend{Downloaded Data, Best Solution}
	\end{axis}

\end{tikzpicture}

%\afterpage{\blankpage}
\input{postamble.tex}	